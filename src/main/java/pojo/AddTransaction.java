package pojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class AddTransaction {
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("effectiveDate")
    @Expose
    private String effectiveDate;
    @SerializedName("money")
    @Expose
    private Money money;
    @SerializedName("source")
    @Expose
    private Source source;
    @SerializedName("transactionTypeCode")
    @Expose
    private String transactionTypeCode;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public String getTransactionTypeCode() {
        return transactionTypeCode;
    }

    public void setTransactionTypeCode(String transactionTypeCode) {
        this.transactionTypeCode = transactionTypeCode;
    }
}
