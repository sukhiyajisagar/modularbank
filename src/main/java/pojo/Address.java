package pojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class Address {
    @SerializedName("addressTypeCode")
    @Expose
    private String addressTypeCode;
    @SerializedName("street1")
    @Expose
    private String street1;
    @SerializedName("street2")
    @Expose
    private String street2;
    @SerializedName("cityCounty")
    @Expose
    private String cityCounty;
    @SerializedName("stateRegion")
    @Expose
    private String stateRegion;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("moveInDate")
    @Expose
    private String moveInDate;

    public String getAddressTypeCode() {
        return addressTypeCode;
    }

    public void setAddressTypeCode(String addressTypeCode) {
        this.addressTypeCode = addressTypeCode;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getCityCounty() {
        return cityCounty;
    }

    public void setCityCounty(String cityCounty) {
        this.cityCounty = cityCounty;
    }

    public String getStateRegion() {
        return stateRegion;
    }

    public void setStateRegion(String stateRegion) {
        this.stateRegion = stateRegion;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMoveInDate() {
        return moveInDate;
    }

    public void setMoveInDate(String moveInDate) {
        this.moveInDate = moveInDate;
    }
}
