package pojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class Source {
    @SerializedName("sourceName")
    @Expose
    private String sourceName;
    @SerializedName("sourceRef")
    @Expose
    private String sourceRef;

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getSourceRef() {
        return sourceRef;
    }

    public void setSourceRef(String sourceRef) {
        this.sourceRef = sourceRef;
    }
}
