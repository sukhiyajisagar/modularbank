package pojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class AddAccount {
    @SerializedName("accountName")
    @Expose
    private String accountName;
    @SerializedName("accountTypeCode")
    @Expose
    private String accountTypeCode;
    @SerializedName("currencyCode")
    @Expose
    private String currencyCode;
    @SerializedName("customerGroupCode")
    @Expose
    private String customerGroupCode;
    @SerializedName("personId")
    @Expose
    private String personId;
    @SerializedName("personName")
    @Expose
    private String personName;
    @SerializedName("priceListTypeCode")
    @Expose
    private String priceListTypeCode;
    @SerializedName("residencyCountryCode")
    @Expose
    private String residencyCountryCode;
    @SerializedName("source")
    @Expose
    private Source source;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountTypeCode() {
        return accountTypeCode;
    }

    public void setAccountTypeCode(String accountTypeCode) {
        this.accountTypeCode = accountTypeCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCustomerGroupCode() {
        return customerGroupCode;
    }

    public void setCustomerGroupCode(String customerGroupCode) {
        this.customerGroupCode = customerGroupCode;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPriceListTypeCode() {
        return priceListTypeCode;
    }

    public void setPriceListTypeCode(String priceListTypeCode) {
        this.priceListTypeCode = priceListTypeCode;
    }

    public String getResidencyCountryCode() {
        return residencyCountryCode;
    }

    public void setResidencyCountryCode(String residencyCountryCode) {
        this.residencyCountryCode = residencyCountryCode;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }
}
