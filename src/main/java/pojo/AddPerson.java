package pojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddPerson {
    @SerializedName("givenName")
    @Expose
    private String givenName;
    @SerializedName("surname")
    @Expose
    private String surname;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("birthDate")
    @Expose
    private String birthDate;
    @SerializedName("personTypeCode")
    @Expose
    private String personTypeCode;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("phoneCountryCode")
    @Expose
    private String phoneCountryCode;
    @SerializedName("educationCode")
    @Expose
    private String educationCode;
    @SerializedName("activityCode")
    @Expose
    private String activityCode;
    @SerializedName("housingTypeCode")
    @Expose
    private String housingTypeCode;
    @SerializedName("buildingTypeCode")
    @Expose
    private String buildingTypeCode;
    @SerializedName("businessAreaCode")
    @Expose
    private String businessAreaCode;
    @SerializedName("maritalStatusCode")
    @Expose
    private String maritalStatusCode;
    @SerializedName("dependantPersons")
    @Expose
    private Integer dependantPersons;
    @SerializedName("employmentTimeCode")
    @Expose
    private String employmentTimeCode;
    @SerializedName("customerType")
    @Expose
    private String customerType;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("placeOfBirth")
    @Expose
    private String placeOfBirth;
    @SerializedName("countryOfBirth")
    @Expose
    private String countryOfBirth;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("taxResidencyCountry")
    @Expose
    private String taxResidencyCountry;
    @SerializedName("fixedEmploymentLength")
    @Expose
    private Integer fixedEmploymentLength;
    @SerializedName("identificationNumber")
    @Expose
    private IdentificationNumber identificationNumber;
    @SerializedName("addresses")
    @Expose
    private List<Address> addresses = null;
    @SerializedName("document")
    @Expose
    private Document document;
    @SerializedName("usResident")
    @Expose
    private Boolean usResident;
    @SerializedName("pep")
    @Expose
    private Boolean pep;

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPersonTypeCode() {
        return personTypeCode;
    }

    public void setPersonTypeCode(String personTypeCode) {
        this.personTypeCode = personTypeCode;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneCountryCode() {
        return phoneCountryCode;
    }

    public void setPhoneCountryCode(String phoneCountryCode) {
        this.phoneCountryCode = phoneCountryCode;
    }

    public String getEducationCode() {
        return educationCode;
    }

    public void setEducationCode(String educationCode) {
        this.educationCode = educationCode;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getHousingTypeCode() {
        return housingTypeCode;
    }

    public void setHousingTypeCode(String housingTypeCode) {
        this.housingTypeCode = housingTypeCode;
    }

    public String getBuildingTypeCode() {
        return buildingTypeCode;
    }

    public void setBuildingTypeCode(String buildingTypeCode) {
        this.buildingTypeCode = buildingTypeCode;
    }

    public String getBusinessAreaCode() {
        return businessAreaCode;
    }

    public void setBusinessAreaCode(String businessAreaCode) {
        this.businessAreaCode = businessAreaCode;
    }

    public String getMaritalStatusCode() {
        return maritalStatusCode;
    }

    public void setMaritalStatusCode(String maritalStatusCode) {
        this.maritalStatusCode = maritalStatusCode;
    }

    public Integer getDependantPersons() {
        return dependantPersons;
    }

    public void setDependantPersons(Integer dependantPersons) {
        this.dependantPersons = dependantPersons;
    }

    public String getEmploymentTimeCode() {
        return employmentTimeCode;
    }

    public void setEmploymentTimeCode(String employmentTimeCode) {
        this.employmentTimeCode = employmentTimeCode;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTaxResidencyCountry() {
        return taxResidencyCountry;
    }

    public void setTaxResidencyCountry(String taxResidencyCountry) {
        this.taxResidencyCountry = taxResidencyCountry;
    }

    public Integer getFixedEmploymentLength() {
        return fixedEmploymentLength;
    }

    public void setFixedEmploymentLength(Integer fixedEmploymentLength) {
        this.fixedEmploymentLength = fixedEmploymentLength;
    }

    public IdentificationNumber getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(IdentificationNumber identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Boolean getUsResident() {
        return usResident;
    }

    public void setUsResident(Boolean usResident) {
        this.usResident = usResident;
    }

    public Boolean getPep() {
        return pep;
    }

    public void setPep(Boolean pep) {
        this.pep = pep;
    }
}
