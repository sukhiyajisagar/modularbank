package pojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IdentificationNumber {
    @SerializedName("idNumber")
    @Expose
    private String idNumber;
    @SerializedName("idCountryCode")
    @Expose
    private String idCountryCode;
    @SerializedName("vatNumber")
    @Expose
    private String vatNumber;
    @SerializedName("taxNumber")
    @Expose
    private String taxNumber;

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getIdCountryCode() {
        return idCountryCode;
    }

    public void setIdCountryCode(String idCountryCode) {
        this.idCountryCode = idCountryCode;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }


}
