package stepDefinations;

import io.cucumber.java.en.Given;
import io.restassured.response.Response;
import resources.APIResources;
import resources.TestDataBuild;
import resources.Utils;

import java.io.IOException;

import static io.restassured.RestAssured.given;

public class StepDefination extends Utils {
    Response response;
    TestDataBuild data = new TestDataBuild();
    static String token;
    static String person_id;
    static String person_name;
    static String guid;
    static String account_id;
    static String currencyCode;

    @Given("Get Token")
    public void getToken() throws IOException {
        APIResources resourceAPI = APIResources.valueOf("AUTHORISE");
        response = given().spec(requestSpecification())
                .body(data.getToken())
                .when().post(resourceAPI.getResource());
        token = getJsonPath(response, "data.token");
        System.out.println("Generated token --> " + getJsonPath(response, "data.token"));
    }

    @Given("Add person")
    public void addPerson() throws IOException {
        APIResources resourceAPI = APIResources.valueOf("ADDPERSON");
        response = given().header("x-auth-token", token).
                header("x-tenant-code", "SANDBOX").
                header("x-channel-code", "SYSTEM").
                header("Content-Type", "application/json").
                body(data.addPerson()).log().all().
                when().post(resourceAPI.getResource());
        person_id = getJsonPath(response, "data.personId");
        person_name = getJsonPath(response, "data.fullName");
        guid = getJsonPath(response, "data.idNumber");
        System.out.println("Generated personId -> " + person_id);
        System.out.println("Generated person name -> " + person_name);
        System.out.println("Generated uniq id -> " + guid);
    }

    @Given("Add account")
    public void addAccount() throws IOException {
        APIResources resourceAPI = APIResources.valueOf("ADDACCOUNT");
        response = given().header("x-auth-token", token).
                header("x-tenant-code", "SANDBOX").
                header("x-channel-code", "SYSTEM").
                header("Content-Type", "application/json").pathParam("personId", person_id)
                .body(data.addAccount(person_id, person_name, guid)).log().all()
                .when().post(resourceAPI.getResource());
        account_id = getJsonPath(response, "data.accountId");
        currencyCode = getJsonPath(response, "data.balances[0].currencyCode");
        System.out.println("Generated Account ID -> " + account_id);
        System.out.println("Currency coe -> " + currencyCode);
    }

    @Given("Get balance")
    public void getBalance() throws IOException {
        APIResources resourceAPI = APIResources.valueOf("GETBALANCE");
        response = given().header("x-auth-token", token).
                header("x-tenant-code", "SANDBOX").
                header("x-channel-code", "SYSTEM").
                header("Content-Type", "application/json").
                pathParam("accountId", account_id).
                queryParam("currencyCode", currencyCode)
                .when().get(resourceAPI.getResource());
        System.out.println("Balance id -> " + getJsonPath(response, "data.balanceId"));
    }

    @Given("Add transaction")
    public void addTransaction() throws IOException {
        APIResources resourceAPI = APIResources.valueOf("ADDTRANSACTION");
        response = given().header("x-auth-token", token).
                header("x-tenant-code", "SANDBOX").
                header("x-channel-code", "SYSTEM").
                header("Content-Type", "application/json").
                pathParam("accountId", account_id).
                body(data.addTransaction()).log().all()
                .when().post(resourceAPI.getResource());
        System.out.println("Account Transaction ID -> " + getJsonPath(response, "data.accountTransactionId"));
    }

}
