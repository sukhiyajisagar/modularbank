package resources;
//enum is special class in java which has collection of constants or  methods
public enum APIResources {
	
	AUTHORISE("/employees/authorise"),
	ADDPERSON("https://person-api.sandbox.modularbank.xyz/api/v1/persons"),
	ADDACCOUNT("https://account-api.sandbox.modularbank.xyz/api/v1/persons/{personId}/accounts"),
	GETBALANCE("https://account-api.sandbox.modularbank.xyz/api/v1/accounts/{accountId}/balances"),
	ADDTRANSACTION("https://account-api.sandbox.modularbank.xyz/api/v3/accounts/{accountId}/transactions");
	private String resource;
	
	APIResources(String resource)
	{
		this.resource=resource;
	}
	
	public String getResource()
	{
		return resource;
	}
	

}
