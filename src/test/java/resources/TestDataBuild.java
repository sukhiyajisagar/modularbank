package resources;

import pojo.*;

import java.util.ArrayList;
import java.util.List;

public class TestDataBuild {

    public GetToken getToken() {
        GetToken p = new GetToken();
        p.setUsername("modular.system");
        p.setPassword("pass");
        return p;
    }

    public AddPerson addPerson() {
        AddPerson p = new AddPerson();
        Address s = new Address();
        Document t = new Document();
        IdentificationNumber q = new IdentificationNumber();
        List<Address> r = new ArrayList<Address>() {
        };
        p.setGivenName("Peter");
        p.setSurname("Schmidt");
        p.setMiddleName("Alexander");
        p.setBirthDate("1980-12-01");
        p.setPersonTypeCode("P");
        p.setSex("M");
        p.setEmail("psmidt@smidt.de");
        p.setPhoneNumber("0901820");
        p.setPhoneCountryCode("+372");
        p.setEducationCode("HIGHER_EDUCATION");
        p.setActivityCode("SPECIALIST");
        p.setHousingTypeCode("PRIVATE");
        p.setBuildingTypeCode("APARTMENT");
        p.setBusinessAreaCode("LEGAL");
        p.setMaritalStatusCode("MARRIED");
        p.setDependantPersons(1);
        p.setEmploymentTimeCode("MORE_4_YEAR");
        p.setCustomerType("string");
        p.setNationality("DE");
        p.setPlaceOfBirth("Berlin");
        p.setCountryOfBirth("DE");
        p.setLanguage("DE");
        p.setTaxResidencyCountry("DE");
        p.setFixedEmploymentLength(6);
        q.setIdNumber("12123456D7"); // Update this value
        q.setIdCountryCode("DE");
        q.setVatNumber("DE123456789");
        q.setTaxNumber("12345678901");
        p.setIdentificationNumber(q);
        s.setAddressTypeCode("R");
        s.setStreet1("Street 1");
        s.setStreet2("Street 2");
        s.setCityCounty("Berlin");
        s.setStateRegion("Berlin");
        s.setZip("13347");
        s.setCountryCode("DE");
        s.setMoveInDate("2018-06-23");
        r.add(s);
        p.setAddresses(r);
        t.setIssuingCountry("DE");
        t.setNumber("0124R5M2S2"); // Update this value
        t.setDocumentTypeCode("PASSPORT");
        t.setExpiryDate("2025-01-04");
        p.setDocument(t);
        p.setUsResident(true);
        p.setPep(true);
        return p;
    }

    public AddAccount addAccount(String person_id, String person_name, String guid) {
        AddAccount p = new AddAccount();
        Source q = new Source();
        p.setAccountName("Demo account");
        p.setAccountTypeCode("CURRENCY");
        p.setCurrencyCode("EUR");
        p.setCustomerGroupCode("GROUP_A");
        p.setPersonId(person_id);
        p.setPersonName(person_name);
        p.setPriceListTypeCode("STANDARD");
        p.setResidencyCountryCode("FI");
        q.setSourceName("TEST");
        q.setSourceRef(guid);
        p.setSource(q);
        return p;
    }

    public AddTransaction addTransaction() {
        AddTransaction p = new AddTransaction();
        Source q = new Source();
        Money r = new Money();
        p.setDetails("Card topup");
        p.setEffectiveDate("2020-06-08");
        r.setAmount(238);
        r.setCurrencyCode("EUR");
        p.setMoney(r);
        q.setSourceName("CARD_TOPUP");
        q.setSourceRef("ID-123");
        p.setSource(q);
        p.setTransactionTypeCode("CARD_TOPUP");
        return p;
    }
}
