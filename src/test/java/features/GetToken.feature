Feature: Validating new account creation

  Background: User want to create new account
    Given Get Token
    And Add person
    And Add account
    And Get balance

  @AddTransaction
  Scenario: User want to add transaction in newly created account
    Given Add transaction